resource "aws_vpc" "vault_vpc" {
  cidr_block       = var.VaultVpcCIDR
  instance_tenancy = "default"
}

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.vault_vpc.id
}
resource "aws_subnet" "public_subnets" {
  vpc_id = aws_vpc.vault_vpc.id


  count = var.HAMode? var.PublicSubnetCount : 2

  cidr_block              = cidrsubnet(aws_vpc.vault_vpc.cidr_block, 4, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true

  assign_ipv6_address_on_creation = false
}
resource "aws_subnet" "private_subnets" {
  vpc_id = aws_vpc.vault_vpc.id
  count  = var.HAMode ? var.PrivateSubnetCount : 2


  cidr_block        = cidrsubnet(aws_vpc.vault_vpc.cidr_block, 4, count.index + var.PublicSubnetCount)
  availability_zone = data.aws_availability_zones.available.names[count.index]

}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vault_vpc.id
  route {
    cidr_block = "0.0.0.0/0" # Traffic from Public Subnet reaches Internet via Internet Gateway
    gateway_id = aws_internet_gateway.IGW.id
  }
}

resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.vault_vpc.id
}

resource "aws_route_table_association" "public_rt_assosciation" {
  count          = var.PrivateSubnetCount
  subnet_id      = element(aws_subnet.public_subnets.*.id, count.index)
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "private_rt_assosciation" {
  count          = var.PrivateSubnetCount
  subnet_id      = element(aws_subnet.private_subnets.*.id, count.index)
  route_table_id = aws_route_table.private_rt.id

}

resource "aws_route" "public_internet_access" {
  route_table_id         = aws_route_table.public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.IGW.id
}
