
# AWS region and AZs in which to deploy
variable "AwsRegion" {
  type = string
}
variable "AwsProfile" {
  type = string
}

variable "EnvironmentName" {
  type = string
}
variable "HAMode" {
  type = bool 
}
  
 

variable "VaultServerNames" {
  description = "Names of the Vault nodes that will join the cluster"
  type        = list(string)

}

variable "VaultServerPrivateIPs" {
  description = "The private ips of the Vault nodes that will join the cluster"
  type        = list(string)
}


# URL for Vault OSS binary
variable "VaultDownloadLink" {
  type = string
}

# Instance size
variable "InstanceType" {
  type = string
}

# SSH key name to access EC2 instances (should already exist) in the AWS Region
variable "KeyPairName" {
}
variable "ClusterName" {
}

variable "VaultVpcCIDR" {}


variable "PrivateSubnetCount" {
  type = number
}
variable "PublicSubnetCount" {
  type = number
}


#"List of CIDR blocks allowed to send requests to your vault endpoint.  Defaults to EVERYWHERE.  You should probably limit this to your organization IP or VPC CIDR."
variable "AllowedTrafficCIDRBlocks" {
  type = list(string)
}
#  "List of IPv6 CIDR blocks allowed to send requests to your vault endpoint.  Defaults to EVERYWHERE.  Set to an empty list if not required."
variable "AllowedTrafficCIDRBlocksIPV6" {
  type = list(string)
}

variable "BoundAmiId" {
  type = string
}
variable "Ec2RoleName" {
  type = string
}
variable "VaultServerIamInstanceProfile" {
  type = string
}

variable "VaultServerIamRole" {
  type = string
}

variable "VaultServerRolePolicy" {
  type = string
}

variable "AmsKeyDeletionWindow" {
  type = number
}

variable "PrivateKeyAlgorithm" {
  type = string
}

variable "LBNamePrefix" {
  type = string
}

variable "LBType" {
  type = string
}
variable "LBTimeOut" {
  type = number
}
variable "LBTagName" {
  type = string
}


variable "LBHealthCheckPath" {
  type = string
}

variable "LBTargetTagName" {
  type = string
}

variable "LBSecurityGroupNamePrefix" {
  type = string
}

variable "S3BucketPrefix" {
  type = string
}

variable "PolicyName" {
  type = string
}

variable "PolicyPath" {
  type = string //secret/example_*
}
variable "PolicyCapabilities" {
  type = list(string)
}
variable "MaxTTL" {
  type = string
}

variable "Domain" {
  type = string
}
variable "SubDomain" {
  type = string
}
variable "HostedZone" {
  type = string
}
