
## Vault Server IAM Config
resource "aws_iam_instance_profile" "vault_server" {
  name = "${var.EnvironmentName}-vault-server-instance-profile"
  role = aws_iam_role.vault_server.name
}

resource "aws_iam_role" "vault_server" {
  name               = "${var.EnvironmentName}-vault-server-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy" "vault_server" {
  name   = "${var.EnvironmentName}-vault-server-role-policy"
  role   = aws_iam_role.vault_server.id
  policy = data.aws_iam_policy_document.vault_server.json
}


data "aws_iam_policy_document" "assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "vault_server" {
  statement {
    sid    = "1"
    effect = "Allow"

    actions = ["ec2:DescribeInstances"]

    resources = ["*"]
  }

  statement {
    sid    = "VaultAWSAuthMethod"
    effect = "Allow"
    actions = [
      "ec2:DescribeInstances",
      "iam:GetInstanceProfile",
      "iam:GetUser",
      "iam:GetRole",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "VaultKMSUnseal"
    effect = "Allow"

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:DescribeKey",
    ]

    resources = ["*"]
  }
}


## S3 Policy
resource "aws_iam_role_policy" "vault_instance_s3_policy" {
  role   = aws_iam_role.vault_server.id
  policy = data.aws_iam_policy_document.s3_vault_policy.json
}

data "aws_iam_policy_document" "s3_vault_policy" {
  statement {
    sid    = "PutObjects"
    effect = "Allow"
    actions = [
      "s3:PutObject",
      "kms:GenerateDataKey",
      "kms:Decrypt"
    ]
    resources = [
      "${aws_s3_bucket.vault_data.arn}/",
      "${aws_s3_bucket.vault_data.arn}/*"
    ]
  }
}
