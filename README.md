# Hashicorp Vault HA cluster on AWS


## Prereqs
* You have a hosted zone in Route 53, e.g. testapp.com

* You have an issued certificate for subdomain.{your-hosted-zone} in AWS Certificate Manager
## AWS

Before starting with Terraform you should have configured your credentials in the AWS folder in your system as shown below.

```aws
[default]
aws_access_key_id =
aws_secret_access_key =
[prod]
aws_access_key_id =
aws_secret_access_key =
```
### Initializing Terraform

```sh
terraform init
```
### Running Terraform

Run the following to ensure ***terraform*** will only perform the expected
actions:

```sh
terraform plan
```
Run the following to ensure ***terraform*** will only perform the expected actions with the  variables from a specific environment file

```sh
terraform plan --var-file="target-file"
```

Run the following to apply the configuration to the target aws Cloud
environment:

```sh
terraform apply
```
Run the following to apply the configuration and read variables from a specific environment file

```sh
terraform apply --var-file="target-file"
```

### Getting Vault Keys
 Encrypted root token and recovery keys will be stored in a s3 bucket. You can decrypt these keys  by executing following command 

    ```shell
    ./temp/vault_credentials.sh
    ```

### Accessing Vault UI

Vault UI can be accessed from  subdomain.{your-hosted-zone}

### Destroy Environment

```sh
terraform destroy
```

 