resource "tls_private_key" "vault_kp" {
  algorithm = var.PrivateKeyAlgorithm
}

resource "null_resource" "vault_kp" {
  provisioner "local-exec" {
    command = "echo \"${tls_private_key.vault_kp.private_key_pem}\" > ${var.KeyPairName}"
  }

  provisioner "local-exec" {
    command = "chmod 600 ${var.KeyPairName}"
  }
}

resource "aws_key_pair" "vault_kp" {
  key_name   = var.KeyPairName
  public_key = tls_private_key.vault_kp.public_key_openssh
}
