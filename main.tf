
terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.AwsRegion

}
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
# 

resource "aws_instance" "vault-server" {
  count                       = var.HAMode ? length(var.VaultServerNames) : 1
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.InstanceType
  subnet_id                   = aws_subnet.public_subnets[count.index % 5].id
  key_name                    = var.KeyPairName
  vpc_security_group_ids      = [aws_security_group.vault_sg.id]
  associate_public_ip_address = true
  # private_ip                  = var.VaultServerPrivateIPs[count.index]
  iam_instance_profile = aws_iam_instance_profile.vault_server.id
  # user_data = data.template_file.vault-server[count.index].rendered
  user_data = templatefile("${path.module}/templates/userdata-vault-server.tpl", {
    tpl_vault_node_name          = var.VaultServerNames[count.index],
    tpl_vault_storage_path       = "/vault/${var.VaultServerNames[count.index]}",
    tpl_vault_zip_file           = var.VaultDownloadLink,
    tpl_vault_service_name       = "vault-${var.EnvironmentName}",
    tpl_vault_node_address_names = zipmap(var.VaultServerPrivateIPs, var.VaultServerNames)
    cluster_name                 = var.ClusterName
    aws_region                   = var.AwsRegion
    aws_kms_key                  = aws_kms_key.vault.id
    bound_ami_id                 = var.BoundAmiId
    ec2_role_name                = var.Ec2RoleName
    vault_s3_bucket_name         = aws_s3_bucket.vault_data.id
    vault_cluster_region         = var.AwsRegion
    vault_kms_key_id             = aws_kms_key.vault.key_id
    policy_name                  = var.PolicyName
    policy_path                  = var.PolicyPath
    policy_capabilities          = jsonencode(var.PolicyCapabilities)
    max_ttl                      = var.MaxTTL
  })

  tags = {
    Name         = "${var.EnvironmentName}-vault-server-${var.VaultServerNames[count.index]}"
    cluster_name = var.ClusterName
  }

  lifecycle {
    ignore_changes = [ami, tags]
  }
}

