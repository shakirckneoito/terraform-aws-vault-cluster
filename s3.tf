// for storing encrypted unseal keys and root token
resource "aws_s3_bucket" "vault_data" {
  bucket_prefix = var.S3BucketPrefix
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags          = { Project = var.EnvironmentName }
  force_destroy = true
}

## S3 Bucket Public Access Block
resource "aws_s3_bucket_public_access_block" "vault_data" {
  bucket                  = aws_s3_bucket.vault_data.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
