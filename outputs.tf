

output "Vault_Instances" {
  value = <<EOF

  vault_2 (${aws_instance.vault-server[0].public_ip}) | internal: (${aws_instance.vault-server[0].private_ip})
    - You can get the recovery keys and root token by running ./temp/vault_credentials.sh 
 
    $ ssh -l ubuntu ${aws_instance.vault-server[0].public_ip} -i ${var.KeyPairName}

    


EOF
}

output "load-balancer-dns-name" {
  description = "dns name of Load balancer "
  value       = aws_lb.alb.dns_name
}


output "VaultUi" {
  description = "Vault UI "
  value       = "${var.SubDomain}.${var.Domain}"
}

# Output the vault credentials script
resource "local_file" "vault_credentials" {
  content = templatefile("${path.root}/templates/creds.tpl", {
    AWS_S3_BUCKET  = aws_s3_bucket.vault_data.id
    AWS_KMS_KEY_ID = aws_kms_key.vault.key_id
    AWS_REGION     = var.AwsRegion
    AWS_PROFILE    = var.AwsProfile
  })
  filename = "${path.root}/temp/vault_credentials.sh"
}

