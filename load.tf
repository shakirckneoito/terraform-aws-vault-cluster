resource "aws_lb" "alb" {
  name_prefix        = var.LBNamePrefix
  internal           = false
  load_balancer_type = var.LBType
  security_groups    = [aws_security_group.load_balancer.id]
  subnets            = aws_subnet.public_subnets.*.id

  idle_timeout = var.LBTimeOut
  #   ip_address_type = var.private_mode ? "ipv4" : "dualstack"

  tags = { Name : var.LBTagName }

}
resource "aws_lb_target_group" "alb_targets" {
  name_prefix          = var.LBNamePrefix
  port                 = 8200
  protocol             = "HTTP"
  vpc_id               = aws_vpc.vault_vpc.id
  deregistration_delay = 30
  target_type          = "instance"

  health_check {
    enabled             = true
    interval            = 10
    path                = var.LBHealthCheckPath
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 3
    unhealthy_threshold = 3
    matcher             = "200"
  }

  tags = { Name : var.LBTargetTagName }


}


resource "aws_lb_listener" "alb_http_redirect" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      host        = "#{host}"
      path        = "/#{path}"
      port        = 443
      protocol    = "HTTPS"
      query       = "#{query}"
      status_code = "HTTP_301"
    }
  }
}
resource "aws_lb_listener" "alb_http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-FS-2018-06"
  certificate_arn   = data.aws_acm_certificate.vault.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_targets.arn
  }
}

resource "aws_security_group" "load_balancer" {
  name_prefix = var.LBSecurityGroupNamePrefix
  vpc_id      = aws_vpc.vault_vpc.id

}


resource "aws_security_group_rule" "load_balancer_allow_80" {
  security_group_id = aws_security_group.load_balancer.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = var.AllowedTrafficCIDRBlocks
  ipv6_cidr_blocks  = length(var.AllowedTrafficCIDRBlocksIPV6) > 0 ? var.AllowedTrafficCIDRBlocksIPV6 : null
  description       = "Allow HTTP traffic."
}

resource "aws_security_group_rule" "load_balancer_allow_443" {
  security_group_id = aws_security_group.load_balancer.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = var.AllowedTrafficCIDRBlocks
  ipv6_cidr_blocks  = length(var.AllowedTrafficCIDRBlocksIPV6) > 0 ? var.AllowedTrafficCIDRBlocksIPV6 : null
  description       = "Allow HTTPS traffic."
}

## Only the Load Balancer is set up to work with IPv6.  Once a request
## comes in, it all goes through IPv4 internally.
resource "aws_security_group_rule" "load_balancer_allow_outbound" {
  security_group_id = aws_security_group.load_balancer.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = length(var.AllowedTrafficCIDRBlocksIPV6) > 0 ? ["::/0"] : null
  description       = "Allow any outbound traffic."
}



resource "aws_alb_target_group_attachment" "alb_gp_attachment" {
  count            = length(var.VaultServerNames)
  target_group_arn = aws_lb_target_group.alb_targets.arn
  target_id        = element(split(",", join(",", aws_instance.vault-server.*.id)), count.index)
}
