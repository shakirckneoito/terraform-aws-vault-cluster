resource "aws_kms_key" "vault" {
  deletion_window_in_days = var.AmsKeyDeletionWindow

  tags = {
    Name = var.ClusterName
  }
}
